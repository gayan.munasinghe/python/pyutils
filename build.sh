#!/bin/bash

PYTHON_PACKAGE_NAME=$(bash package.sh name)
PACKAGE_NAME=$(echo $PYTHON_PACKAGE_NAME | awk -F '-' '{print $2}')
VERSION=$(bash package.sh version)
URL=$(grep URL *.spec | awk '{print $2}')
SUMMARY=$(grep Summary *.spec | awk '{print $2}')
cp -rf ${PACKAGE_NAME} ${PACKAGE_NAME}-${VERSION}
tar -czvf ${PYTHON_PACKAGE_NAME}-${VERSION}.tar.gz ${PACKAGE_NAME}-${VERSION}
cp ${PYTHON_PACKAGE_NAME}-${VERSION}.tar.gz ~/rpmbuild/SOURCES/
PACKAGE_NAME=${PACKAGE_NAME} \
VERSION=${VERSION} \
URL=${URL} \
SUMMARY=${SUMMARY} \
rpmbuild -bb ${PYTHON_PACKAGE_NAME}.spec
