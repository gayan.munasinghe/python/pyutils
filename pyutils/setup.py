import setuptools
import os

PACKAGE_NAME=os.environ['PACKAGE_NAME']
SUMMARY=os.environ['SUMMARY']
VERSION=os.environ['VERSION']
URL=os.environ['URL']
INSTALL_REQUIRES=os.environ['INSTALL_REQUIRES'].split(',')

setuptools.setup(
    name=PACKAGE_NAME,
    version=VERSION,
    author='Gayan Munasinghe',
    author_email='gayan@outlook.com',
    description=SUMMARY,
    url=URL,
    packages=[PACKAGE_NAME],
    license='MIT',
    install_requires=INSTALL_REQUIRES,
    python_requires='>=3.6',
)
