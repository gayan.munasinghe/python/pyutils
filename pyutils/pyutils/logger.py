"""
loggeing module for the google_api library. All logs are supressed by default.
"""
import logging

logging.getLogger().addHandler(logging.NullHandler())
LOGGER = logging.getLogger()
