"""
A simple Option Parser inspired by docopt.
docopt has become really slow for some reason. Decided to write my own.
"""
import sys
import re

class gopt:
    """
    This classes parses a string document options and arguments and stores them
    in  dictionary.

    Consider the following doc-string
    doc = '''
        Usage:
            -c, --config    Configuration file [default:cfg/config.json]
    '''

    Initialize the gopt object and access opts dictionary.

    print(gopt(doc).opts)

    Examples:

    Input: script.py
    Output: {'-c': 'cfg/config.json', '--config': 'cfg/config.json'}

    Input: script.py -c test.json
    Output: {'-c': 'test.json', '--config': 'test.json'}

    Input: script.py --config test.json
    Output: {'-c': 'test.json', '--config': 'test.json'}

    Input: script.py --config
    Output: {'-c': 'True', '--config': 'True'}

    Input: script.py -c
    Output: {'-c': 'True', '--config': 'True'}

    """
    def __init__(self, doc):
        self.args = sys.argv[1:]
        self.__help(doc)
        self.opts = {}
        self.p = re.compile(r'(\s*(-[a-zA-Z])?,?\s*(--[a-zA-Z]*)?.*default:([0-9a-zA-Z-/._]*)|\s*(-[a-zA-Z])?,?\s*(--[a-zA-Z]*)?.*)') # pylint:disable=line-too-long
        opt_doc = self.__get_option_string(doc)
        self.__lines(opt_doc)

    @staticmethod
    def __get_option_string(opt_doc):
        p = re.compile(r'[\s\S]*(OPTIONS:[\S\s]*)')
        m = p.match(opt_doc)
        return m.group(1)

    def __help(self, doc):
        if '-h' in self.args or '--help' in self.args:
            print(doc)
            sys.exit(0)

    def __add_values(self, short_opt, long_opt, value):
        if short_opt:
            self.opts[short_opt] = value
        if long_opt:
            self.opts[long_opt] = value

    def __add_options(self, short_opt, long_opt, default):
        if short_opt not in self.args and long_opt not in self.args:
            if default:
                self.__add_values(short_opt, long_opt, default)
            else:
                self.__add_values(short_opt, long_opt, None)
            return
        idx = self.__get_index(short_opt, long_opt)
        if len(self.args) >= idx+2:
            if self.args[idx+1][0] != '-':
                self.__add_values(short_opt, long_opt, self.args[idx+1])
                return
        self.__add_values(short_opt, long_opt, True)

    def __get_index(self, short_opt, long_opt):
        """ long_opt has priority """
        if long_opt in self.args:
            return self.args.index(long_opt)
        return self.args.index(short_opt)

    def __lines(self, doc):
        lines = doc.split('\n')
        for line in lines:
            self.__line(line)

    def __line(self, line):
        m = self.p.match(line)
        short_opt = m.group(2) or m.group(5)
        long_opt = m.group(3) or m.group(6)
        default = m.group(4)
        self.__add_options(short_opt, long_opt, default)
