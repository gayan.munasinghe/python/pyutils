#!/usr/bin/python3

""" This module contains a Mixture of useful python utilities """

import json
import os
import subprocess
import sys
import yaml

from pyutils import logger

def read_json(json_file_name):
    """ Reads a json file and returns the content """
    return json.load(open(json_file_name, encoding='utf-8'))

def read_yaml(yaml_file_name):
    """ Reads a yaml file and returns the content """
    with open(yaml_file_name, encoding='utf-8') as stream:
        return yaml.safe_load(stream)

def __get_config_type(config_file):
    return config_file.split('.')[-1]

def load_config(config_file, config_type=None):
    """
    Use to load different types of config files.
    Defualt is yaml.
    """
    config_type = config_type or __get_config_type(config_file)
    if config_type == 'json':
        return read_json(config_file)
    return read_yaml(config_file)

def create_base_dirs(file_names):
    """
    Creates the base directories for file_names.
    """
    file_names = [file_names] if isinstance(file_names, str) else file_names
    for file_name in file_names:
        dir_name = os.path.dirname(file_name)
        create_dirs(dir_name)

def create_dirs(dir_names):
    """
    Creates the directory list.
    """
    dir_names = [dir_names] if isinstance(dir_names, str) else dir_names
    for dir_name in dir_names:
        os.makedirs(dir_name, exist_ok=True)

def py_bash(bash_command, out=None):
    """
    Runs a bash command.
    Generally can accept pipes. A pipe needs to be separated by two spaces ' | '.
    To save the stdout to a file pass a file handler as 'out'.
    """
    out = out or sys.stdout
    commands = bash_command.split(' | ')
    if len(commands) > 1:
        iout = subprocess.Popen(commands[0].split(), stdout=subprocess.PIPE) # pylint: disable=consider-using-with
        for command in commands[1:-1]:
            iout = subprocess.Popen(command.split(), stdin=iout.stdout, stdout=subprocess.PIPE) # pylint: disable=consider-using-with
        output = subprocess.check_call(commands[-1].split(), stdin=iout.stdout, stdout=out)
    else:
        output = subprocess.check_call(commands[0].split(), stdout=out)
    return output

def _enabled(dic):
    return dic.get('enabled', True)

def enabled(dic, name='dictionary'):
    """
    Checks whether a certain dictionary is enabled, by checking for 'enabled' keyword.
    Default is set to true.
    """
    if _enabled(dic):
        logger.LOGGER.debug("%s is enabled. Continuing.", name)
        return True
    logger.LOGGER.debug("%s is not enabled. Skipping.", name)
    return False

def modify_keys(obj, convert):
    """
    Recursively goes through the dictionary obj and replaces keys with the convert function.
    """
    if isinstance(obj, (str, int, float)):
        return obj
    if isinstance(obj, dict):
        new = obj.__class__()
        for k, v in obj.items():
            new[convert(k)] = modify_keys(v, convert)
    elif isinstance(obj, (list, set, tuple)):
        new = obj.__class__(modify_keys(v, convert) for v in obj)
    else:
        return obj
    return new

def extract_key(d):
    """
    extracts the key of the first key value pair of dictionary 'd'.
    """
    return next(iter(d))

def json_extract(data, where):
    """ Extracts data from a json dictionary
        A = {"A": { "B": { "C": "c"}}}
        json_extract(data, "A") gives {"B": {"C": "c"}}
        json_extract(data, "A,B") gives {"C": "c"}
        json_extract(data, "A,B,C") gives "c"
    """
    depth = where.split(',')
    value = data
    for item in depth:
        value = value[item]
    return value

def files(dirname): # pylint: disable=redefined-outer-name
    """
    retuns a list of files in dirname with absolute file paths.
    """
    (absdir, _ , files) = next(os.walk(os.path.abspath(dirname)))
    absfiles = [f"{absdir}/{file}" for file in files]
    return absfiles
