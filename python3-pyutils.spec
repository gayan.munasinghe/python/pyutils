%define name %(echo $(${CI_PROJECT_DIR}/package.sh name))
%define version %(echo $(${CI_PROJECT_DIR}/package.sh version))
%define release %(echo $(${CI_PROJECT_DIR}/package.sh release))
%global modname %(echo %{name} | awk -F '-' '{print $2}')

Name:           %{name}
Version:        %{version}
Release:        %{release}%{?dist}
Summary:        python utilities
License:        MIT
URL:            https://gitlab.crowdnoetic.com/python/pyutils
Source0:        %{name}-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  python3-devel
BuildRequires:  python3-rpm-macros
BuildRequires:  python3-setuptools
BuildRequires:  python3-six
Requires:       python3-pyyaml

%?python_enable_dependency_generator

%description
Wrapper for logging

%prep
%autosetup -n %{modname}-%{version}

%build
%py3_build

%install
%py3_install

%check

%files -n python3-%{modname}
%{python3_sitelib}/%{modname}/
%{python3_sitelib}/%{modname}-%{version}*

%changelog
* Fri Mar 24 2023 Gayan Munasinghe <gayan@outlook.com>
  - Initial push to gitlab.crowdnoetic.com
